#ifndef BLOC_H
#define BLOC_H
#include <iostream>


class bloc
{
    public:
        bloc();   //constructeur
        bloc(int x,int y); //constructeur surchargé
        ~bloc(); //destructeur
        //getteur
        int getposx() const;
        int getposy() const;
        //setteur
        void setposx(int x);
        void setposy(int y);
    protected:
        int posx; //postion du bloc
        int posy;
};

#endif // BLOC_H
