#include "jeu.h"
#include "niveau.h"



Jeu::Jeu()
{
    m_motDePasse1="AllerAuNiveau2";
    m_motDePasse2="AllerAuNiveau3";
}

Jeu::~Jeu()
{
    //dtor
}

void Jeu::affichageMenuPrinc(int choix, Console* pConsole)
{
            pConsole->setColor(COLOR_DEFAULT);
            if(choix==1) pConsole->setColor(COLOR_BLUE);
            pConsole->gotoLigCol(5, 37);
            cout<<"JOUER";

            pConsole->setColor(COLOR_DEFAULT);
            if(choix==2) pConsole->setColor(COLOR_BLUE);
            pConsole->gotoLigCol(6, 31);
            cout<<"CHARGER UNE PARTIE";

            pConsole->setColor(COLOR_DEFAULT);
            if(choix==3) pConsole->setColor(COLOR_BLUE);
            pConsole->gotoLigCol(7, 34);
            cout<<"MOT DE PASSE";

            pConsole->setColor(COLOR_DEFAULT);
            if(choix==4) pConsole->setColor(COLOR_BLUE);
            pConsole->gotoLigCol(8, 37);
            cout<<"SCORES";

            pConsole->setColor(COLOR_DEFAULT);
            if(choix==5) pConsole->setColor(COLOR_BLUE);
            pConsole->gotoLigCol(9, 36);
            cout<<"QUITTER";
            pConsole->setColor(COLOR_DEFAULT);
            pConsole->gotoLigCol(0, 0);
}

void Jeu::chargerPartie(Console* pConsole)
{
    string NomDeCompte;
    pConsole->gotoLigCol(5, 21);
    cout<<"Veuillez inscrire votre nom de compte";
    pConsole->gotoLigCol(6, 21);
    cin>>NomDeCompte;
}

void Jeu::motDePasse(Console* pConsole)
{
    string motDePasse;
    pConsole->gotoLigCol(5, 29);
    cout<<"Entrer un mot de passe";
    pConsole->gotoLigCol(6, 29);
    cin>>motDePasse;
    if(motDePasse==m_motDePasse1)
    {
        pConsole->gotoLigCol(7, 20);
        cout<<"Mot de passe bon, vous voici au niveau 2";
    }
    if(motDePasse==m_motDePasse2)
    {
        pConsole->gotoLigCol(7, 20);
        cout<<"Mot de passe bon, vous voici au niveau 3";
    }
    if(motDePasse!=m_motDePasse1 && motDePasse!=m_motDePasse2)
    {
        pConsole->gotoLigCol(7, 29);
        cout<<"Mot de passe incorrect";
    }
    pConsole->gotoLigCol(8, 20);
    system("pause");
}

void Jeu::score(Console* pConsole)
{
    pConsole->gotoLigCol(5, 31);
    cout<<"Voici vos scores :";
    pConsole->gotoLigCol(6, 33);
    cout<<"Niveau 1 :";
    pConsole->gotoLigCol(7, 33);
    cout<<"Niveau 2 :";
    pConsole->gotoLigCol(8, 33);
    cout<<"Niveau 3 :";
    pConsole->gotoLigCol(8, 20);
    system("pause");
}


void Jeu::menu ()
{
    Console* pConsole = NULL;
    pConsole = Console::getInstance();
    bool quit=false;
    int choix=1;
            affichageMenuPrinc(choix,pConsole);

    while (!quit)
    {
        if (pConsole->isKeyboardPressed())
        {
            char a = pConsole->getInputKey();
            if ((a=='s' || a==80) && choix<5) choix++;
            if ((a=='z' || a==72) && choix>1) choix--;

            affichageMenuPrinc(choix,pConsole);
            if (a == 13)
            {
                if (choix==1)
                {
                    system("cls");
                    Niveau test;
                    test.bouclejeu();
                    system("cls");
                    affichageMenuPrinc(choix,pConsole);
                }
                if (choix==2)
                {
                    system("cls");
                    chargerPartie(pConsole);
                    system("cls");
                    affichageMenuPrinc(choix,pConsole);
                }

                if (choix==3)
                {
                    system("cls");
                    motDePasse(pConsole);
                    system("cls");
                    affichageMenuPrinc(choix,pConsole);
                }
                if (choix==4)
                {
                    system("cls");
                    score(pConsole);
                    system("cls");
                    affichageMenuPrinc(choix,pConsole);
                }
                if (choix == 5)
                {
                    quit = true;
                    pConsole->gotoLigCol(13, 0);
                }
            }
        }

    }
}

