#ifndef BAMOVIBLE_H
#define BAMOVIBLE_H
#include <iostream>
#include "bloc.h"

class Bamovible : public bloc
{
    public:
        Bamovible(int x, int y);
        ~Bamovible();
        bool getpousse() const;
        void setpousse(bool a);

    private:
        bool pousse;
};

#endif // BAMOVIBLE_H
