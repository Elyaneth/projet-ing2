#ifndef OISEAU_H_INCLUDED
#define OISEAU_H_INCLUDED
#include <iostream>

class Oiseau
{
private:
    int m_posxoiseau;
    int m_posyoiseau;

public:
    Oiseau();
    Oiseau(int posxoiseau,int posyoiseau);
    ~Oiseau();

     int getxoiseau();
     int getyoiseau();

};


#endif // OISEAU_H_INCLUDED
