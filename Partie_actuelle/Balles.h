#ifndef BALLES_H_INCLUDED
#define BALLES_H_INCLUDED
#include <iostream>

class Balle
{
private:
    int m_posxballe;
    int m_posyballe;

    int m_vitx;
    int m_vity;

public:
    Balle();
    ~Balle();

     int getxballe();
     int getyballe();
     void setposx(int _posx);
     void setposy(int _posy);
     void initballex();
     void initballey();

     int getvitx();
     int getvity();
     void setvitx(int _vitx);
     void setvity(int _vity);


};



#endif // BALLES_H_INCLUDED
