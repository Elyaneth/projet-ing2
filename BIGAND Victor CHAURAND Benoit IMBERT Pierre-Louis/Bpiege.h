#ifndef BPIEGE_H
#define BPIEGE_H
#include <iostream>
#include "bloc.h"

class Bpiege : public bloc
{
    public:
        Bpiege(int x,int y); //constructeur surcharg�
        virtual ~Bpiege();   //destructeur

        bool getexplose() const; //getteur
        void setexplose(bool a); //setteur

    private:
        bool explose;     //bool�en pour savoir si le bloc a d�j� explos�
};

#endif // BPIEGE_H
