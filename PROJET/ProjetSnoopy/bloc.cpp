#include "bloc.h"
using namespace std;

bloc::bloc()
    :posx(0),posy(0)
{
    //ctor
}

bloc::bloc(int x,int y)
    :posx(x),posy(y)
{

}

int bloc::getposx() const
{
    return posx;
}

int bloc::getposy() const
{
    return posy;
}

void bloc::setposx(int x)
{
    if ((posx>=0)&&(posx)<=20)
    {
        posx=x;
    }
}

void bloc::setposy(int y)
{
    if ((posy>=0)&&(posy)<=10)
    {
        posy=y;
    }
}

bloc::~bloc()
{
    //dtor
}
