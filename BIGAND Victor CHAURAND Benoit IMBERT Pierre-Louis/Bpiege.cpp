#include "Bpiege.h"

Bpiege::Bpiege(int x,int y)
        :bloc(x,y),explose(false) //initialise la pos et le booleen d'explosion a fasle
{
    //ctor
}

Bpiege::~Bpiege()
{
    //dtor
}

bool Bpiege::getexplose() const
{
    return explose;
}

void Bpiege::setexplose(bool a)
{
    explose=a;
}
