#ifndef BAMOVIBLE_H
#define BAMOVIBLE_H
#include <iostream>
#include "bloc.h"

class Bamovible : public bloc
{
    public:
        Bamovible(int x, int y); //constructeur surcharg�
        ~Bamovible();            //destructeur
        bool getpousse() const;  //getteur du bool�en de pouss�
        void setpousse(bool a);  //setteur de bool�en de pouss�

    private:
        bool pousse;        //bool�en pour savoir si le bloc a d�j� �t� pouss� ou pas
};

#endif // BAMOVIBLE_H
