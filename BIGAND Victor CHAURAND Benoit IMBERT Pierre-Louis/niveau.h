#ifndef NIVEAU_H_INCLUDED
#define NIVEAU_H_INCLUDED
#include "Joueur.h"
#include <iostream>
#include <fstream>
#include <conio.h>
#include <vector>
#include "Bamovible.h"
#include "bcassable.h"
#include "Bpiege.h"
#include "Oiseau.h"
#include "Balles.h"
#include "ctime"
#include "console.h"
#include "Cstdlib"

class Niveau
{
    private:
        int m_vie; //nombre de vie restant
        int m_lvl; //Niveau actuel
        int m_score; //score actuel sur le niveau
        char m_tab[21][11]; //matrice
        Balle m_balle; //cr�er la balle
        clock_t temps; //gestion du temmps restant
        int m_tempsmax; //variable sauvegardant le temps restant apres sauvegarde
        std::vector<Oiseau>matO; //vecteur oiseau
        int m_nbOiseau;  //nombre d'oiseau restant � attraper
        std::vector<Bamovible>matP; //vecteur bloc amovible
        std::vector<bcassable>matC; //vecteur bloc cassable
        std::vector<Bpiege>matT;   //vecteur bloc pi�g�

    public:

        Niveau(int lvl);  //constructeur surcharg�
        ~Niveau();       //destructeur
        //getteur
        char gettab(int posx, int posy);
        int getscore();
        int getvie() const;
        int getnboiseau() const;
        //setteur
        void settab(int posx, int posy, char lettre);
        void setnboiseau();
        void setvie();
        //affiche la matrice
        void affichage(Console* pConsole);
        //gere la boucle de jeu
        void bouclejeu();
        //initialise la matrice
        void initiationmatrice(joueur snoopy);
        //gere le deplacement du joeur
        void depjoueur(char key, joueur& snoopy);
        //gere les actions entre les blocs et le joeur
        void actionbloc(joueur& snoopy,char key);
        //gere le deplacement de la balle
        void deplacementballe(joueur& snoopy);
        //sauvegarde la matrice avec les vies restantes, le temps et le score
        void sauvegarde(int temps);
};

#endif // NIVEAU_H_INCLUDED

