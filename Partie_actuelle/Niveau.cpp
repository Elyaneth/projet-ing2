#include "Niveau.h"
#include "console.h"
#include <iostream>
using namespace std;

Niveau::Niveau(int lvl)

{
    temps=1;
    m_tempsmax=60;
    m_lvl=lvl;
    m_vie=3;
    m_nbOiseau=0;
    Balle m_balle;
    m_score=0;
    if(lvl==1)
    {
        ifstream fic1("niveau1.txt", ios::in);  // on ouvre le fichier en lecture
        char c; //char qui va lire un par un les caract�res du fichier qui contient la matrice de d�part!
        if(fic1)  // si l'ouverture a r�ussi
        {
            for(unsigned j=0; j<10; j++)
            {
                for (unsigned i=0; i<20;i++)
                {
                    fic1.get(c);
                    settab(i,j,c);
                    switch (c)
                        {
                            case('O'):  matO.push_back(Oiseau(i,j));
                                        m_nbOiseau++;
                                        break;
                            case('P'):  matP.push_back(Bamovible(i,j));
                                        break;
                            case('T'):  matT.push_back(Bpiege(i,j));
                                        break;
                            case('C'):  matC.push_back(bcassable(i,j));
                                        break;
                            default:    break;
                        }
                }
            }
        fic1.close();
        }
    }
    else if(lvl==2)
    {
        ifstream fic2("niveau2.txt", ios::in);  // on ouvre le fichier en lecture
        char c; //char qui va lire un par un les caract�res du fichier qui contient la matrice de d�part!
        if(fic2)  // si l'ouverture a r�ussi
        {
            for(unsigned j=0; j<10; j++)
            {
                for (unsigned i=0; i<20;i++)
                {
                    fic2.get(c);
                    settab(i,j,c);
                    switch (c)
                        {
                            case('O'):  matO.push_back(Oiseau(i,j));
                                        m_nbOiseau++;
                                        break;
                            case('P'):  matP.push_back(Bamovible(i,j));
                                        break;
                            case('T'):  matT.push_back(Bpiege(i,j));
                                        break;
                            case('C'):  matC.push_back(bcassable(i,j));
                                        break;
                            default:    break;
                        }
                }
            }
        fic2.close();
        }
    }
    else if(lvl==3)
    {
        ifstream fic3("niveau3.txt", ios::in);  // on ouvre le fichier en lecture
        char c; //char qui va lire un par un les caract�res du fichier qui contient la matrice de d�part!
        if(fic3)  // si l'ouverture a r�ussi
        {
            for(unsigned j=0; j<10; j++)
            {
                for (unsigned i=0; i<20;i++)
                {
                    fic3.get(c);
                    settab(i,j,c);
                    switch (c)
                        {
                            case('O'):  matO.push_back(Oiseau(i,j));
                                        m_nbOiseau++;
                                        break;
                            case('P'):  matP.push_back(Bamovible(i,j));
                                        break;
                            case('T'):  matT.push_back(Bpiege(i,j));
                                        break;
                            case('C'):  matC.push_back(bcassable(i,j));
                                        break;
                            default:    break;
                        }
                }
            }
        fic3.close();
        }
    }
        else if(lvl==4)
        {
            ifstream ficinfo("infosauvegarde.txt",ios::in);
            ifstream fictab("tabsauvegarde.txt",ios::in);
            char c;
            if(ficinfo&&fictab)
            {
                for(unsigned j=0; j<10; j++)
                {
                    for (unsigned i=0; i<20;i++)
                    {
                        fictab.get(c);
                        settab(i,j,c);
                        switch (c)
                        {
                            case('O'):  matO.push_back(Oiseau(i,j));
                                        m_nbOiseau++;
                                        break;
                            case('P'):  matP.push_back(Bamovible(i,j));
                                        break;
                            case('T'):  matT.push_back(Bpiege(i,j));
                                        break;
                            case('C'):  matC.push_back(bcassable(i,j));
                                        break;
                            default:    break;
                        }
                    }
                }
                ficinfo>>m_lvl>>m_vie>>m_tempsmax;
                fictab.close();
                ficinfo.close();
            }
            else
            {
                std::cout<<std::endl<<std::endl<<std::endl<<std::endl<<std::endl<<std::endl<<std::endl<<std::endl<<std::endl<<std::endl<<std::endl;
                std::cout<<"Fichier de sauvegarde inexistant..";
            }
        }
}

Niveau::~Niveau()
{

}

int Niveau::getscore()
{
    return m_score;
}

char Niveau::gettab(int posx, int posy)
{
    return m_tab[posx][posy];
}

void Niveau::settab(int posx, int posy, char lettre)
{
    m_tab[posx][posy]=lettre;
}

int Niveau::getvie() const
{
    return m_vie;
}

int Niveau::getnboiseau() const
{
    return m_nbOiseau;
}

void Niveau::setnboiseau()
{
    if((m_nbOiseau>0)&&(m_nbOiseau<=4))
    {
        m_nbOiseau--;
    }
}

void Niveau::setvie()
{
    m_balle.initballex();
    m_balle.initballey();

    m_vie--;
}

void Niveau::affichage(Console* pConsole)
{
    for(unsigned j=0; j<11; j++)
    {

        for (unsigned i=0; i<21;i++)
        {
            if(i==20) settab(i,j,'|');
            if(j==10) settab(i,j,'-');
            if(gettab(i,j)=='S') pConsole->setColor(COLOR_GREEN);
            if(gettab(i,j)=='B') pConsole->setColor(COLOR_BLUE);
            if(gettab(i,j)=='P') pConsole->setColor(COLOR_PURPLE);
            if(gettab(i,j)=='T') pConsole->setColor(COLOR_RED);
            if(gettab(i,j)=='C') pConsole->setColor(COLOR_YELLOW);
            cout << gettab(i,j);
            pConsole->setColor(COLOR_DEFAULT);
        }
        cout << endl;
    }
    pConsole->gotoLigCol(4,25);
    cout<<"Vie restante : "<<getvie();
    pConsole->gotoLigCol(5,25);
    cout<<"Nombre d'oiseaux restants: "<<getnboiseau();

}

void Niveau::initiationmatrice(joueur snoopy)
{
    settab(snoopy.getposx(),snoopy.getposy(),'S');
}


void Niveau::depjoueur(char key, joueur& snoopy)
{
      if (key=='d')
        {
            if ((snoopy.getposx()<19))
            {
                if (m_tab[snoopy.getposx()+1][snoopy.getposy()]==' ')
                {
                    snoopy.setposx(1);
                    settab(snoopy.getposx(), snoopy.getposy(),'S');
                    settab(snoopy.getposx()-1, snoopy.getposy(),' ');
                }
                else
                    actionbloc(snoopy,key);
            }
        }
        if (key=='z')
        {
            if (snoopy.getposy()>0)
            {
                if (m_tab[snoopy.getposx()][snoopy.getposy()-1]==' ')
                {
                    snoopy.setposy(-1);
                    settab(snoopy.getposx(), snoopy.getposy(),'S');
                    settab(snoopy.getposx(), snoopy.getposy()+1,' ');
                }
                else
                    actionbloc(snoopy,key);
            }
        }
        if (key=='q')
        {
            if (snoopy.getposx()>0)
            {
                if (m_tab[snoopy.getposx()-1][snoopy.getposy()]==' ')
                {
                    snoopy.setposx(-1);
                    settab(snoopy.getposx(), snoopy.getposy(),'S');
                    settab(snoopy.getposx()+1, snoopy.getposy(),' ');
                }
                else
                    actionbloc(snoopy,key);
            }
        }
        if (key=='s')
        {
            if (snoopy.getposy()<9)
            {
                if (m_tab[snoopy.getposx()][snoopy.getposy()+1]==' ')
                {
                    snoopy.setposy(1);
                    settab(snoopy.getposx(), snoopy.getposy(),'S');
                    settab(snoopy.getposx(), snoopy.getposy()-1,' ');
                }
                else
                    actionbloc(snoopy,key);
            }
        }
        if(key=='a')
        {
            actionbloc(snoopy,key);
        }
}

void Niveau::deplacementballe(joueur& snoopy)
{
    if (m_balle.getxballe()+m_balle.getvitx()==snoopy.getposx() && m_balle.getyballe()+m_balle.getvity()==snoopy.getposy())
        {
            settab(m_balle.getxballe(),m_balle.getyballe(),' ');
            setvie();
        }

     else if (m_balle.getxballe()+m_balle.getvitx()==snoopy.getposx() && m_balle.getyballe()==snoopy.getposy())
        {
            settab(m_balle.getxballe(),m_balle.getyballe(),' ');
            setvie();
        }
     else if  (m_balle.getxballe()==snoopy.getposx() && m_balle.getyballe()+m_balle.getvity()==snoopy.getposy())
        {
            settab(m_balle.getxballe(),m_balle.getyballe(),' ');
            setvie();
        }
    else
        {

            if (gettab(m_balle.getxballe()+m_balle.getvitx(),m_balle.getyballe())!=' '&& gettab(m_balle.getxballe()+m_balle.getvitx(),m_balle.getyballe())!='S') m_balle.setvitx(m_balle.getvitx());
            if (gettab(m_balle.getxballe(),m_balle.getyballe()+m_balle.getvity())!=' '&& gettab(m_balle.getxballe(),m_balle.getyballe()+m_balle.getvity())!='S') m_balle.setvity(m_balle.getvity());

            if (gettab(m_balle.getxballe()+m_balle.getvitx(),m_balle.getyballe()+m_balle.getvity())!=' ' && gettab(m_balle.getxballe()+m_balle.getvitx(),m_balle.getyballe()+m_balle.getvity())!='S')
            {
                m_balle.setvitx(m_balle.getvitx());
                m_balle.setvity(m_balle.getvity());
            }
            m_balle.setposx(m_balle.getvitx());
            m_balle.setposy(m_balle.getvity());


            settab(m_balle.getxballe(), m_balle.getyballe(),'B');
            settab(m_balle.getxballe()-m_balle.getvitx(), m_balle.getyballe()-m_balle.getvity(),' ');
            if (m_balle.getxballe()==snoopy.getposx() && m_balle.getyballe()==snoopy.getposy())
                {
                    settab(m_balle.getxballe(),m_balle.getyballe(),' ');
                    setvie();
                }

            if ( m_balle.getxballe() == 19)
            {
             m_balle.setvitx(m_balle.getvitx());
            }
            if ( m_balle.getxballe() == 0)
            {
             m_balle.setvitx(m_balle.getvitx());
            }
            if ( m_balle.getyballe() == 9)
            {
             m_balle.setvity(m_balle.getvity());
            }
            if ( m_balle.getyballe() == 0 )
            {
             m_balle.setvity(m_balle.getvity());
            }
            if (gettab(m_balle.getxballe()+m_balle.getvitx(),m_balle.getyballe())!=' '&& gettab(m_balle.getxballe()+m_balle.getvitx(),m_balle.getyballe())!='S') m_balle.setvitx(m_balle.getvitx());
            if (gettab(m_balle.getxballe(),m_balle.getyballe()+m_balle.getvity())!=' '&& gettab(m_balle.getxballe(),m_balle.getyballe()+m_balle.getvity())!='S') m_balle.setvity(m_balle.getvity());
            if (gettab(m_balle.getxballe()+m_balle.getvitx(),m_balle.getyballe()+m_balle.getvity())!=' ' && gettab(m_balle.getxballe()+m_balle.getvitx(),m_balle.getyballe()+m_balle.getvity())!='S')
            {
                m_balle.setvitx(m_balle.getvitx());
                m_balle.setvity(m_balle.getvity());
            }
        }
}

void Niveau::actionbloc(joueur& snoopy,char key)
{
        std::vector<Bamovible>::iterator itP;
        std::vector<bcassable>::iterator itC;
        std::vector<Bpiege>::iterator itT;
        std::vector<Oiseau>::iterator itO;

        switch(key)
        {
            case('d'):  if(m_tab[snoopy.getposx()+1][snoopy.getposy()]=='P')
                        {
                            for(itP=matP.begin();itP!=matP.end();itP++)
                            {
                                if((itP->getposx()==snoopy.getposx()+1)&&(itP->getpousse()==false)&&(itP->getposy()==snoopy.getposy())&&(gettab(snoopy.getposx()+2,snoopy.getposy())==' '))
                                {
                                    itP->setpousse(true); //le bloc ne pourra plus �tre pouss�
                                    itP->setposx(snoopy.getposx()+2);
                                    settab(snoopy.getposx()+2,snoopy.getposy(),'P');
                                    settab(snoopy.getposx()+1,snoopy.getposy(),' ');

                                }
                            }
                        }
                        if(m_tab[snoopy.getposx()+1][snoopy.getposy()]=='B')
                        {
                            settab(m_balle.getxballe(),m_balle.getyballe(),' ');
                            setvie();
                        }

                        if(m_tab[snoopy.getposx()+1][snoopy.getposy()]=='T')
                        {
                            for(itT=matT.begin();itT!=matT.end();itT++)
                            {
                                if((itT->getposx()==snoopy.getposx()+1)&&(itT->getposy()==snoopy.getposy())&&(itT->getexplose()==false))
                                {
                                    itT->setexplose(true);
                                    settab(m_balle.getxballe(),m_balle.getyballe(),' ');
                                    setvie();
                                    settab(snoopy.getposx()+1,snoopy.getposy(),' ');
                                }
                            }
                        }
                        if(m_tab[snoopy.getposx()+1][snoopy.getposy()]=='O')
                        {
                            for(itO=matO.begin();itO!=matO.end();itO++)
                            {
                                if((itO->getxoiseau()==snoopy.getposx()+1)&&(itO->getyoiseau()==snoopy.getposy())&&itO->getrecup()==false)
                                {
                                    itO->setrecup(true);
                                    settab(snoopy.getposx()+1,snoopy.getposy(),' ');
                                    setnboiseau();

                                }
                            }
                        }
                        break;

            case('z'):  if(m_tab[snoopy.getposx()][snoopy.getposy()-1]=='P')
                        {
                            for(itP=matP.begin();itP!=matP.end();itP++)
                            {
                                if((itP->getposx()==snoopy.getposx())&&(itP->getpousse()==false)&&(itP->getposy()==snoopy.getposy()-1)&&(gettab(snoopy.getposx(),snoopy.getposy()-2)==' '))
                                {
                                    itP->setpousse(true);
                                    itP->setposy(snoopy.getposy()-2);
                                    settab(snoopy.getposx(),snoopy.getposy()-2,'P');
                                    settab(snoopy.getposx(),snoopy.getposy()-1,' ');
                                }
                            }
                        }
                        if(m_tab[snoopy.getposx()+1][snoopy.getposy()]=='B')
                        {
                            settab(m_balle.getxballe(),m_balle.getyballe(),' ');
                            setvie();
                        }
                        if(m_tab[snoopy.getposx()][snoopy.getposy()-1]=='T')
                        {
                            for(itT=matT.begin();itT!=matT.end();itT++)
                            {
                                if((itT->getposx()==snoopy.getposx())&&(itT->getposy()==snoopy.getposy()-1)&&(itT->getexplose()==false))
                                {
                                    itT->setexplose(true);
                                    settab(m_balle.getxballe(),m_balle.getyballe(),' ');
                                    setvie();
                                    settab(snoopy.getposx(),snoopy.getposy()-1,' ');
                                }
                            }
                        }
                        if(m_tab[snoopy.getposx()][snoopy.getposy()-1]=='O')
                        {
                            for(itO=matO.begin();itO!=matO.end();itO++)
                            {
                                if((itO->getxoiseau()==snoopy.getposx())&&(itO->getyoiseau()==snoopy.getposy()-1)&&itO->getrecup()==false)
                                {
                                    itO->setrecup(true);
                                    settab(snoopy.getposx(),snoopy.getposy()-1,' ');
                                    setnboiseau();

                                }
                            }
                        }
                        break;
            case('q'):  if(m_tab[snoopy.getposx()-1][snoopy.getposy()]=='P')
                        {
                            for(itP=matP.begin();itP!=matP.end();itP++)
                            {
                                if((itP->getposx()==snoopy.getposx()-1)&&(itP->getpousse()==false)&&(itP->getposy()==snoopy.getposy())&&(gettab(snoopy.getposx()-2,snoopy.getposy())==' '))
                                {
                                    itP->setpousse(true);
                                    itP->setposx(snoopy.getposx()-2);
                                    settab(snoopy.getposx()-2,snoopy.getposy(),'P');
                                    settab(snoopy.getposx()-1,snoopy.getposy(),' ');
                                }
                            }
                        }
                        if(m_tab[snoopy.getposx()+1][snoopy.getposy()]=='B')
                        {
                            settab(m_balle.getxballe(),m_balle.getyballe(),' ');
                            setvie();
                        }
                        if(m_tab[snoopy.getposx()-1][snoopy.getposy()]=='T')
                        {
                            for(itT=matT.begin();itT!=matT.end();itT++)
                            {
                                if((itT->getposx()==snoopy.getposx()-1)&&(itT->getposy()==snoopy.getposy())&&(itT->getexplose()==false))
                                {
                                    itT->setexplose(true);
                                    settab(m_balle.getxballe(),m_balle.getyballe(),' ');
                                    setvie();
                                    settab(snoopy.getposx()-1,snoopy.getposy(),' ');
                                }
                            }
                        }
                        if(m_tab[snoopy.getposx()-1][snoopy.getposy()]=='O')
                        {
                            for(itO=matO.begin();itO!=matO.end();itO++)
                            {
                                if((itO->getxoiseau()==snoopy.getposx()-1)&&(itO->getyoiseau()==snoopy.getposy())&&itO->getrecup()==false)
                                {
                                    itO->setrecup(true);
                                    settab(snoopy.getposx()-1,snoopy.getposy(),' ');
                                    setnboiseau();

                                }
                            }
                        }
                        break;
            case('s'):  if(m_tab[snoopy.getposx()][snoopy.getposy()+1]=='P')
                        {
                            for(itP=matP.begin();itP!=matP.end();itP++)
                            {
                                if((itP->getposx()==snoopy.getposx())&&(itP->getpousse()==false)&&(itP->getposy()==snoopy.getposy()+1)&&(gettab(snoopy.getposx(),snoopy.getposy()+2)==' '))
                                {
                                    itP->setpousse(true);
                                    itP->setposy(snoopy.getposy()+2);
                                    settab(snoopy.getposx(),snoopy.getposy()+2,'P');
                                    settab(snoopy.getposx(),snoopy.getposy()+1,' ');
                                }
                            }
                        }
                        if(m_tab[snoopy.getposx()+1][snoopy.getposy()]=='B')
                        {
                            settab(m_balle.getxballe(),m_balle.getyballe(),' ');
                            setvie();
                        }
                        if(m_tab[snoopy.getposx()][snoopy.getposy()+1]=='T')
                        {
                            for(itT=matT.begin();itT!=matT.end();itT++)
                            {
                                if((itT->getposx()==snoopy.getposx())&&(itT->getposy()==snoopy.getposy()+1)&&(itT->getexplose()==false))
                                {
                                    itT->setexplose(true);
                                    settab(m_balle.getxballe(),m_balle.getyballe(),' ');
                                    setvie();
                                    settab(snoopy.getposx(),snoopy.getposy()+1,' ');
                                }
                            }
                        }
                        if(m_tab[snoopy.getposx()][snoopy.getposy()+1]=='O')
                        {
                            for(itO=matO.begin();itO!=matO.end();itO++)
                            {
                                if((itO->getxoiseau()==snoopy.getposx())&&(itO->getyoiseau()==snoopy.getposy()+1)&&itO->getrecup()==false)
                                {
                                    itO->setrecup(true);
                                    settab(snoopy.getposx(),snoopy.getposy()+1,' ');
                                    setnboiseau();

                                }
                            }
                        }
                        break;
            case('a'):  if(gettab(snoopy.getposx(),snoopy.getposy()+1)=='C')
                        {
                            for(itC=matC.begin();itC!=matC.end();itC++) //cas des blocs cassables
                            {
                                if((itC->getposx()==snoopy.getposx())&&(itC->getposy()==snoopy.getposy()+1)&&(itC->getcasse()==false))
                                {
                                    itC->setcasse(true);
                                    settab(snoopy.getposx(),snoopy.getposy()+1,' ');
                                }
                            }
                        }
                        if(gettab(snoopy.getposx(),snoopy.getposy()-1)=='C')
                        {
                            for(itC=matC.begin();itC!=matC.end();itC++) //cas des blocs cassables
                            {
                                if((itC->getposx()==snoopy.getposx())&&(itC->getposy()==snoopy.getposy()-1)&&(itC->getcasse()==false))
                                {
                                    itC->setcasse(true);
                                    settab(snoopy.getposx(),snoopy.getposy()-1,' ');
                                }
                            }
                        }
                        if(gettab(snoopy.getposx()+1,snoopy.getposy())=='C')
                        {
                            for(itC=matC.begin();itC!=matC.end();itC++) //cas des blocs cassables
                            {
                                if((itC->getposx()==snoopy.getposx()+1)&&(itC->getposy()==snoopy.getposy())&&(itC->getcasse()==false))
                                {
                                    itC->setcasse(true);
                                    settab(snoopy.getposx()+1,snoopy.getposy(),' ');
                                }
                            }
                        }
                        if(gettab(snoopy.getposx()-1,snoopy.getposy())=='C')
                        {
                            for(itC=matC.begin();itC!=matC.end();itC++) //cas des blocs cassables
                            {
                                if((itC->getposx()==snoopy.getposx()-1)&&(itC->getposy()==snoopy.getposy())&&(itC->getcasse()==false))
                                {
                                    itC->setcasse(true);
                                    settab(snoopy.getposx()-1,snoopy.getposy(),' ');
                                }
                            }
                        }
                        break;
            default:    break;
        }



}
void Niveau::sauvegarde(int temps)
{
    ofstream ficinfo("infosauvegarde.txt", ios::out | ios::trunc); //fichier sauvegarde infos - �criture
    ofstream fictab("tabsauvegarde.txt", ios::out | ios::trunc); //fichier sauvegarde matrice - �criture
    ifstream ficscore("Mscoresauvegarde.txt", ios::in); //fichiers meilleurs scores
    int score=0;
    if(ficinfo&&fictab)
    {
        for(unsigned j=0;j<10;j++)
        {
            for(unsigned i=0;i<20;i++)
            {
                fictab.put(gettab(i,j));
            }
        }
        ficinfo<<m_lvl<<std::endl;
        ficinfo<<m_vie<<std::endl;
        ficinfo<<temps;
        ficinfo.close();
        fictab.close();
    }
    if(ficscore)
    {
        switch(m_lvl)
        {
            case(1):    ficscore>>score;
                        break;
            case(2):    ficscore>>score;
                        break;
            case(3):    ficscore>>score;
                        break;
            default:    break;
        }
        ficscore.close();
    }
    else
    {
        std::cerr<<"ERR FICHIER";
    }

    ofstream ficscore2("Mscoresauvegarde.txt",ios::out | ios::ate);
    if(ficscore2)
    {
        switch(m_lvl)
        {
            case(1):    if(m_score>score)
                        {
                            ficscore2<<m_score;
                        }
                        break;
            default:    break;

        }
        ficscore2.close();
    }
}

void Niveau::bouclejeu()
{
     Console* pConsole = NULL;
     joueur snoopy;
     int score=0;
    bool quit = false;
    pConsole = Console::getInstance();
    clock_t tempon2=clock()/CLOCKS_PER_SEC;
    int tempon=0;
    initiationmatrice(snoopy);
    affichage(pConsole);
    int depballe=0;

 while (!quit)
    {
        temps=m_tempsmax-(clock()/CLOCKS_PER_SEC)+tempon2;
       if(tempon!=(int)temps)
       {
           pConsole->gotoLigCol(3,25);
        if((int)temps<10) cout<<"Temps restant : 0"<<(int) temps;
       else cout<<"Temps restant : "<<(int) temps;
       score=(int)temps*100;
       pConsole->gotoLigCol(2,25);
       cout<<"Score niveau "<<m_lvl<<": "<<score;
       tempon=(int)temps;
       }

       if ( temps==0 || getvie()==0)
            {
                quit = true;
                pConsole->gotoLigCol(12,0);
            }
        pConsole->gotoLigCol(snoopy.getposy(), snoopy.getposx());
        depballe++;
        if(depballe>4000)
        {
            deplacementballe(snoopy);
            pConsole->gotoLigCol(0,0);
            affichage(pConsole);
            depballe=0;
        }
         pConsole->gotoLigCol(snoopy.getposy(), snoopy.getposx());
        // Si on a appuy� sur une touche du clavier
        if (pConsole->isKeyboardPressed())
        {

            char key = pConsole->getInputKey();
            if (key=='d' || key=='z' || key=='q' || key=='s' || key=='a') depjoueur(key, snoopy);

            pConsole->gotoLigCol(0, 0);
            affichage(pConsole);
            pConsole->gotoLigCol(snoopy.getposy(),snoopy.getposx());

            if (key == 27) // 27 = touche escape
            {
                quit = true;
                pConsole->gotoLigCol(12,0);
                sauvegarde(temps);
            }
        }
        if (m_balle.getxballe()==snoopy.getposx() && m_balle.getyballe()==snoopy.getposy())
            {
                settab(m_balle.getxballe(),m_balle.getyballe(),' ');
                setvie();
            }

        if (getnboiseau()==0)
        {
            if(m_lvl==1)
            {
            m_score=score;
           system("cls");
           pConsole->gotoLigCol(5, 31);
           cout<<"NIVEAU FINI !!!!!";
           pConsole->gotoLigCol(6, 19);
           cout<<"Mot de passe pour niveau 2: 123456";
            }
            if(m_lvl==2)
            {
            m_score=score;
           system("cls");
           pConsole->gotoLigCol(5, 31);
           cout<<"NIVEAU FINI !!!!!";
           pConsole->gotoLigCol(6, 17);
           cout<<"Mot de passe pour niveau 3: readytoROCK";
            }
            if(m_lvl==3) m_score=score;
            while (!pConsole->isKeyboardPressed())
           {
               quit = true;
               pConsole->gotoLigCol(12,0);
           }
        }
    }

    // Lib�re la m�moire du pointeur !
    Console::deleteInstance();
}


