#ifndef JEU_H
#define JEU_H
#include <iostream>
#include "console.h"
#include "Cstdlib"
#include "niveau.h"

using namespace std;
class Jeu
{
    private:
        string m_motDePasse1;  //mot de passe pour acceder directement au niveau 2 et 3
        string m_motDePasse2;
        int m_score1; //meilleur score du niveau 1,2 et 3
        int m_score2;
        int m_score3;

    public:
        Jeu(); //constructeur
        ~Jeu(); //destructeur
        void menu();  //m�thode g�rant le menu
        void affichageMenuPrinc(int choix, Console* pConsole); //affiche le menu
        int motDePasse(Console* pConsole); //gere les mots de passes permettant de pass� au niveau 2 et 3
        void score(Console* pConsole); //affiche les meilleurs score des 3 niveaux
};

#endif // JEU_H
