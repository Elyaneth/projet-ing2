#ifndef BCASSABLE_H
#define BCASSABLE_H
#include <iostream>
#include "bloc.h"

class bcassable : public bloc
{
    public:
        bcassable(int x,int y); //constructeur surcharg�
        ~bcassable();           //destructeur

        bool getcasse() const; //getteur
        void setcasse(bool a); //setteur

    private:
        bool casse;  //bool�en pour savoir si le bloc est cass� ou pas

};

#endif // BCASSABLE_H
