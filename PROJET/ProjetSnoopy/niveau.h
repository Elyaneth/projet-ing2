#ifndef NIVEAU_H_INCLUDED
#define NIVEAU_H_INCLUDED
#include "Joueur.h"
#include <iostream>
#include <fstream>
#include <conio.h>
#include <vector>
#include "Bamovible.h"
#include "bcassable.h"
#include "Bpiege.h"
#include "Oiseau.h"
#include "Balles.h"

#include "console.h"


class Niveau
{
    private:
        char m_tab[21][11];
        std::vector<Oiseau>matO;
        std::vector<Bamovible>matP;
        std::vector<bcassable>matC;
        std::vector<Bpiege>matT;

    public:

        Niveau();
        ~Niveau();
        char gettab(int posx, int posy);
        void settab(int posx, int posy, char lettre);
        void affichage(Console* pConsole);
        void bouclejeu();
        void initiationmatrice(joueur snoopy);
        void depjoueur(char key, joueur& snoopy);

        void deplacementballe(char key, Balle& balletest);
};

#endif // NIVEAU_H_INCLUDED

