#include "Bpiege.h"

Bpiege::Bpiege(int x,int y)
        :bloc(x,y),explose(false)
{
    //ctor
}

Bpiege::~Bpiege()
{
    //dtor
}

bool Bpiege::getexplose() const
{
    return explose;
}

void Bpiege::setexplose(bool a)
{
    explose=a;
}
