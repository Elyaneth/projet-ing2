#ifndef NIVEAU_H_INCLUDED
#define NIVEAU_H_INCLUDED

#include <iostream>
class Niveau
{
private:
    char m_tab[21][11];

public:
    Niveau();
    ~Niveau();
    char** gettab(int posx, int posy);
    void settab(int posx, int posy);
    void affichage();
};

#endif // NIVEAU_H_INCLUDED
