#include "Niveau.h"
#include "console.h"
#include <iostream>
using namespace std;


Niveau::Niveau()

{
    ifstream fic("niveau1.txt", ios::in);  // on ouvre le fichier en lecture

    char c; //char qui va lire un par un les caract�res du fichier qui contient la matrice de d�part!
    if(fic)  // si l'ouverture a r�ussi
    {
        for(unsigned j=0; j<10; j++)
        {
            for (unsigned i=0; i<20;i++)
            {
                fic.get(c);
                settab(i,j,c);
                switch (c)
                    {
                        case('O'):  matO.push_back(Oiseau(i,j));
                                    break;
                        case('P'):  matP.push_back(Bamovible(i,j));
                                    break;
                        case('T'):  matT.push_back(Bpiege(i,j));
                                    break;
                        case('C'):  matC.push_back(bcassable(i,j));
                                    break;
                        default:    break;
                    }
            }
        }
        fic.close();
    }
}

Niveau::~Niveau()
{

}

char Niveau::gettab(int posx, int posy)
{
    return m_tab[posx][posy];
}

void Niveau::settab(int posx, int posy, char lettre)
{
    m_tab[posx][posy]=lettre;
}

void Niveau::affichage(Console* pConsole)
{
    for(unsigned j=0; j<11; j++)
    {

        for (unsigned i=0; i<21;i++)
        {
            if(j==7)
            {
                pConsole->gotoLigCol(j,i);
            }
            if(i==20) settab(i,j,'|');
            if(j==10) settab(i,j,'-');
            if(gettab(i,j)=='S') pConsole->setColor(COLOR_GREEN);
            if(gettab(i,j)=='O') pConsole->setColor(COLOR_BLUE);
            if(gettab(i,j)=='P') pConsole->setColor(COLOR_PURPLE);
            if(gettab(i,j)=='T') pConsole->setColor(COLOR_RED);
            if(gettab(i,j)=='C') pConsole->setColor(COLOR_YELLOW);
            cout << gettab(i,j);
            pConsole->setColor(COLOR_DEFAULT);
        }
        cout << endl;
    }


}

void Niveau::initiationmatrice(joueur snoopy)
{
    settab(snoopy.getposx(),snoopy.getposy(),'S');
}


void Niveau::depjoueur(char key, joueur& snoopy)
{
      if (key=='d')
        {
            if ((snoopy.getposx()<19)&&(gettab(snoopy.getposx()+1,snoopy.getposy())==' '))
            {
                snoopy.setposx(1);
                settab(snoopy.getposx(), snoopy.getposy(),'S');
                settab(snoopy.getposx()-1, snoopy.getposy(),' ');
            }
        }
        if (key=='z')
        {
            if ((snoopy.getposy()>0)&&(m_tab[snoopy.getposx()][snoopy.getposy()-1]==' '))
            {
                snoopy.setposy(-1);
                settab(snoopy.getposx(), snoopy.getposy(),'S');
                settab(snoopy.getposx(), snoopy.getposy()+1,' ');
            }
        }
        if ((key=='q')&&(m_tab[snoopy.getposx()-1][snoopy.getposy()]==' '))
        {
            if (snoopy.getposx()>0)
            {
                snoopy.setposx(-1);
                settab(snoopy.getposx(), snoopy.getposy(),'S');
                settab(snoopy.getposx()+1, snoopy.getposy(),' ');
            }
        }
        if ((key=='s')&&(m_tab[snoopy.getposx()][snoopy.getposy()+1]==' '))
        {
            if (snoopy.getposy()<9)
            {
                snoopy.setposy(1);
                settab(snoopy.getposx(), snoopy.getposy(),'S');
                settab(snoopy.getposx(), snoopy.getposy()-1,' ');
            }
        }
}

void Niveau::deplacementballe(char key, Balle& balletest)
{


        balletest.setposx(balletest.getvitx());
        balletest.setposy(balletest.getvity());


        settab(balletest.getxballe(), balletest.getyballe(),'B');
        settab(balletest.getxballe()-balletest.getvitx(), balletest.getyballe()-balletest.getvity(),' ');



              if (key =='o')
        {

        if ( balletest.getxballe() == 19)
        {
         balletest.setvitx(balletest.getvitx());
        }
        if ( balletest.getxballe() == 0)
        {
         balletest.setvitx(balletest.getvitx());
        }
        if ( balletest.getyballe() == 9)
        {
         balletest.setvity(balletest.getvity());
        }
        if ( balletest.getyballe() == 0 )
        {
         balletest.setvity(balletest.getvity());
        }
        }
    }



void Niveau::bouclejeu()
{
     Console* pConsole = NULL;
     joueur snoopy;
     Balle balletest;

    bool quit = false;
    pConsole = Console::getInstance();

    initiationmatrice(snoopy);
    affichage(pConsole);

 while (!quit)
    {
        pConsole->gotoLigCol(snoopy.getposy(), snoopy.getposx());

        // Si on a appuy� sur une touche du clavier
        if (pConsole->isKeyboardPressed())
        {

            char key = pConsole->getInputKey();
            if (key=='d' || key=='z' || key=='q' || key=='s') depjoueur(key, snoopy);

            if (key=='o') deplacementballe(key, balletest);

            pConsole->gotoLigCol(0, 0);
            affichage(pConsole);
            pConsole->gotoLigCol(snoopy.getposy(),snoopy.getposx());
            if (key == 27) // 27 = touche escape
            {
                quit = true;
                pConsole->gotoLigCol(12,0);

            }
        }
    }

    // Lib�re la m�moire du pointeur !
    Console::deleteInstance();
}


