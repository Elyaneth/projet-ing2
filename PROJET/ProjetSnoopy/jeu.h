#ifndef JEU_H
#define JEU_H
#include <iostream>
#include "console.h"
#include "Cstdlib"
#include "niveau.h"

using namespace std;
class Jeu
{
    private:
        string m_motDePasse1;
        string m_motDePasse2;

    public:
        Jeu();
        ~Jeu();
        void menu();
        void chargerPartie(Console* pConsole);
        void affichageMenuPrinc(int choix, Console* pConsole);
        void motDePasse(Console* pConsole);
        void score(Console* pConsole);
};

#endif // JEU_H
