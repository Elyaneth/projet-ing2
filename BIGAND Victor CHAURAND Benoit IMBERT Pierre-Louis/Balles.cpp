#include "Balles.h"
#include <iostream>
using namespace std;

Balle::Balle()
{
    //initiialise la position et la vittesse la balle
    m_posxballe=2;
    m_posyballe=1;

    m_vitx=1;
    m_vity=1;
}

Balle::~Balle()
{
    //rien a faire, pas d'espace a liberer
}


int Balle::getxballe()
{
    return m_posxballe;
}

int Balle::getyballe()
{
    return m_posyballe;
}


void Balle::setposx(int _posx)
{
    m_posxballe+=_posx;
}

void Balle::setposy(int _posy)
{
    m_posyballe+=_posy;
}

void Balle::initballex()
{
    m_posxballe=2;
    m_vitx=1;
}

void Balle::initballey()
{
    m_posyballe=1;
    m_vity=1;
}


int Balle::getvitx()
{
    return m_vitx;
}

int Balle::getvity()
{
    return m_vity;
}


void Balle::setvitx(int _vitx)
{
    m_vitx =  -m_vitx;
}

void Balle::setvity(int _vity)
{
    m_vity =  -m_vity;
}
