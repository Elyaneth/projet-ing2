#include "Joueur.h"
#include <iostream>
using namespace std;

joueur::joueur()
{
    m_pos_x=10;
    m_pos_y=5;    //Snoopy arrive au milieu de la matrice
    m_vie=3;
}

joueur::~joueur()
{

}

int joueur::getposx()
{
    return m_pos_x;
}

int joueur::getposy()
{
    return m_pos_y;
}

int joueur::getvie()
{
    return m_vie;
}

void joueur::setposx(int _posx)
{
    m_pos_x+=_posx;
}

void joueur::setposy(int _posy)
{
    m_pos_y+=_posy;
}

void joueur::setvie()
{
    m_vie=m_vie-1;
}


