#ifndef NIVEAU_H_INCLUDED
#define NIVEAU_H_INCLUDED
#include "Joueur.h"
#include <iostream>
#include <fstream>
#include <conio.h>
#include <vector>
#include "Bamovible.h"
#include "bcassable.h"
#include "Bpiege.h"
#include "Oiseau.h"
#include "Balles.h"
#include "ctime"
#include "console.h"
#include "Cstdlib"

class Niveau
{
    private:
        int m_vie;
        int m_lvl;
        int m_score;
        char m_tab[21][11];
        Balle m_balle;
        clock_t temps;
        int m_tempsmax;
        std::vector<Oiseau>matO;
        int m_nbOiseau;
        std::vector<Bamovible>matP;
        std::vector<bcassable>matC;
        std::vector<Bpiege>matT;

    public:

        Niveau(int lvl);
        ~Niveau();
        char gettab(int posx, int posy);
        void settab(int posx, int posy, char lettre);
        int getscore();
        int getvie() const;
        int getnboiseau() const;
        void setnboiseau();
        void setvie();
        void affichage(Console* pConsole);
        void bouclejeu();
        void initiationmatrice(joueur snoopy);
        void depjoueur(char key, joueur& snoopy);
        void actionbloc(joueur& snoopy,char key);
        void deplacementballe(char key, joueur& snoopy);
        void sauvegarde(int temps);
};

#endif // NIVEAU_H_INCLUDED

