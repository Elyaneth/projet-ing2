#ifndef BLOC_H
#define BLOC_H
#include <iostream>


class bloc
{
    public:
        bloc();
        bloc(int x,int y);
        ~bloc();
        int getposx() const;
        int getposy() const;
        void setposx(int x);
        void setposy(int y);
    protected:
        int posx;
        int posy;
    private:
};

#endif // BLOC_H
