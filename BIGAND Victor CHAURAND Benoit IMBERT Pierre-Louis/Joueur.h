#ifndef JOUEUR_H_INCLUDED
#define JOUEUR_H_INCLUDED
#include <iostream>
class joueur
{
private:
    int m_pos_x; //postion du joueur
    int m_pos_y;
    int m_vie; //nombre de vie restant au joueur

public:
    joueur(); //constructeur
    ~joueur(); //destructeur
    //getteur
    int getposx();
    int getposy();
    int getvie();
    //setteur
    void setposx(int _posx);
    void setposy(int _posy);
    void setvie();
};

#endif // JOUEUR_H_INCLUDED
