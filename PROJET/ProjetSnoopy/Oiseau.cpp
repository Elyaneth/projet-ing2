#include "Oiseau.h"
#include <iostream>
using namespace std;

Oiseau::Oiseau()
{
    m_posxoiseau=1;
    m_posyoiseau=1;
}

//constructeur par surcharge
Oiseau::Oiseau(int posxoiseau,int posyoiseau)
{
    if( (posxoiseau>=0)&&(posyoiseau>=0) )
    {
        m_posxoiseau=posxoiseau;
        m_posyoiseau=posyoiseau;
    }
    else
    {std::cout << "Valeurs  oiseau invalides" << std::endl;

    }

}


Oiseau::~Oiseau()
{
    //rien a faire, pas d'espace a liberer
}


int Oiseau::getxoiseau()
{
    return m_posxoiseau;
}

int Oiseau::getyoiseau()
{
    return m_posyoiseau;
}
