#ifndef OISEAU_H_INCLUDED
#define OISEAU_H_INCLUDED
#include <iostream>

class Oiseau
{
private:
    int m_posxoiseau;
    int m_posyoiseau;
    bool m_recup;

public:
    Oiseau();
    Oiseau(int posxoiseau,int posyoiseau);
    ~Oiseau();

     int getxoiseau() const;
     int getyoiseau() const;
     bool getrecup()  const;
     void setrecup(bool a);

};


#endif // OISEAU_H_INCLUDED
