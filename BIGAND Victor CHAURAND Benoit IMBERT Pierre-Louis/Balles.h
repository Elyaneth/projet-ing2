#ifndef BALLES_H_INCLUDED
#define BALLES_H_INCLUDED
#include <iostream>

class Balle
{
private:
    int m_posxballe;
    int m_posyballe;

    int m_vitx;
    int m_vity;   //Positions de la balle dans la matrice et sa vittesse en x et en y

public:
    Balle();  //constructeur
    ~Balle();   //destructeur

     int getxballe();       //getteur de la position en x et y de la balle
     int getyballe();
     void setposx(int _posx); //setteur de la postion en x et y de la balle
     void setposy(int _posy);
     void initballex();         //Remet la balle a sa position de depart quand le joeur perd une vie en x et y
     void initballey();

     int getvitx();         //getteur de la vittesse de la balle
     int getvity();
     void setvitx(int _vitx); //setteur vittesse de la balle
     void setvity(int _vity);


};



#endif // BALLES_H_INCLUDED
