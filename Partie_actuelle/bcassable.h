#ifndef BCASSABLE_H
#define BCASSABLE_H
#include <iostream>
#include "bloc.h"

class bcassable : public bloc
{
    public:
        bcassable(int x,int y);
        ~bcassable();

        void dropbonus(); //a mettre en place plus tard

        bool getcasse() const;
        void setcasse(bool a);

    private:
        bool casse;

};

#endif // BCASSABLE_H
