#ifndef JOUEUR_H_INCLUDED
#define JOUEUR_H_INCLUDED
#include <iostream>
class joueur
{
private:
    int m_pos_x;
    int m_pos_y;
    int m_vie;
    int m_bonus;   //numero suivant le bonus

public:
    joueur();
    ~joueur();
    int getposx();
    int getposy();
    int getvie();
    int getbonus();
    void setposx(int _posx);
    void setposy(int _posy);
    void setvie();
    void setbonus(int _bonus);
    //void deplacement();
};

#endif // JOUEUR_H_INCLUDED
