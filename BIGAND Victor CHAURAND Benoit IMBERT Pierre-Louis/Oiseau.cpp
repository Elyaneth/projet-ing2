#include "Oiseau.h"
#include <iostream>
using namespace std;

Oiseau::Oiseau()
{
    m_posxoiseau=1;
    m_posyoiseau=1;
}

//constructeur par surcharge
Oiseau::Oiseau(int posxoiseau,int posyoiseau)
{
    bool m_recup=false;
    if( (posxoiseau>=0)&&(posyoiseau>=0) )
    {
        m_posxoiseau=posxoiseau;
        m_posyoiseau=posyoiseau;
    }
    else
    {std::cout << "Valeurs  oiseau invalides" << std::endl;

    }

}


Oiseau::~Oiseau()
{
    //rien a faire, pas d'espace a liberer
}


int Oiseau::getxoiseau() const
{
    return m_posxoiseau;
}

int Oiseau::getyoiseau() const
{
    return m_posyoiseau;
}

bool Oiseau::getrecup() const
{
    return m_recup;
}

void Oiseau::setrecup(bool a)
{
    if (m_recup==false)
    {
        m_recup=a;
    }
}
