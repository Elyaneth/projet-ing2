#ifndef BPIEGE_H
#define BPIEGE_H
#include <iostream>
#include "bloc.h"

class Bpiege : public bloc
{
    public:
        Bpiege(int x,int y);
        virtual ~Bpiege();

        bool getexplose() const;
        void setexplose(bool a);

    private:
        bool explose;
};

#endif // BPIEGE_H
