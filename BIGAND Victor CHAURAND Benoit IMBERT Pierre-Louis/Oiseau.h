#ifndef OISEAU_H_INCLUDED
#define OISEAU_H_INCLUDED
#include <iostream>

class Oiseau
{
private:
    int m_posxoiseau; //pos de l'oiseau
    int m_posyoiseau;
    bool m_recup; //v�riie si il a d�j� �t� r�cup�r�

public:
    Oiseau();
    Oiseau(int posxoiseau,int posyoiseau); //constructeur et constructeur surcharg�
    ~Oiseau();//destructeur
//getteur et setteur
     int getxoiseau() const;
     int getyoiseau() const;
     bool getrecup()  const;
     void setrecup(bool a);

};


#endif // OISEAU_H_INCLUDED
